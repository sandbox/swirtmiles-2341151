/**
 * Use this then run through jscompress.com to generate the minified version.
 *
 *  @author Steve Wirt
 */
var hookJS = hookJS || {}
hookJS.hook = hookJS.hook || {}
// Create the hookJS container to store any hooks if they don't already exist
hookJS.hook.screenStateChangePre =  (typeof hookJS.hook.screenStateChangePre === 'object' ) ? hookJS.hook.screenStateChangePre : {};
hookJS.hook.screenStateChange =  (typeof hookJS.hook.screenStateChange === 'object' ) ? hookJS.hook.screenStateChange : {};

var urlParams = {};
(function () {
    var match,
        pl     = /\+/g,  // Regex for replacing addition symbol with a space
        search = /([^&=]+)=?([^&]*)/g,
        decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
        query  = window.location.search.substring(1);

    while (match = search.exec(query))
       urlParams[decode(match[1])] = decode(match[2]);
})();


;(function($) {
    $(document).ready(function(){
		//Set getKey method for finding key for a specific bracket.
        Drupal.settings.screensize.bracket.getKey = function(value){
        for(var key in this){
            if(this[key] == value){
              return key;
            }
          }
          return null;
     };
        // CHECK FOR screen size TO SET  BODY data and Drupal.settings.screensize.current
		mmgBlockThrowerSetScreenSize();
       // Continue to update the screensize if window is adjusted
       $(window).resize(mmgBlockThrowerSetScreenSize);

    })
})(jQuery);

;function consoleMe(sMessage) {
     if ((urlParams["debug"] === 'blocks') || (urlParams["debug"] === 'block')) {
        console.log (sMessage);
     }
};

;function RunThrowCalls(){
    if (!!Drupal.settings.local_responsive.throwCalls){
        //There are some thowCalls to re-run
        for( var blockIDkey in Drupal.settings.local_responsive.throwCalls ){
            eval(Drupal.settings.local_responsive.throwCalls[blockIDkey]);
        }
    }
};


;function mmgBlockThrowerSetScreenSize (){
 	//Ensure global _gaq Google Analytics queue has been initialized.
 	if (typeof _gaq === 'undefined') {
 		_gaq = [];
 	}

    var screenSizeNew = Drupal.settings.screensize.bracket.getKey(0) || 'mobile';//sets minimum default in case all logic fails
    var aSubBrackets = new Array();
    if (!!Drupal.settings.screensize.bracket) {
        //Loop through bracket states to determine which applies currently (race condition, last one to fit the size wins)
        for( var key in Drupal.settings.screensize.bracket ){
            if (jQuery(window).width() >= Drupal.settings.screensize.bracket[key]) {
                screenSizeNew = key;
                aSubBrackets.push(key + '-up');
            }
        }
       //unset the last value of the array
       aSubBrackets.pop();
       //implode the array.
       sSubBrackets = aSubBrackets.join(' ');
    }//end check for bracket

	//check to see if this is the first init or a resize
	if (Drupal.settings.screensize.initialized != "TRUE") {
	    //this is the first time this has been run
	    jQuery('html').attr('data-screensize', screenSizeNew +' '+ sSubBrackets);
        Drupal.settings.screensize.current = screenSizeNew;
        Drupal.settings.screensize.initialized = "TRUE";
        //track the layout
        _gaq.push(["_trackEvent", "Site Layout",'responsive-layout-'+screenSizeNew,,, true]);
        consoleMe('Initialized as: ' + screenSizeNew);
        RunThrowCalls();
	} else {
	    //this is being run on resize
	    if(Drupal.settings.screensize.current  != screenSizeNew) {
            //Means there has been a change from one screen state to another
            var sOldScreenState = Drupal.settings.screensize.current
            //update data in both locations
            consoleMe('Changing screenstate from '+sOldScreenState+' to : ' + screenSizeNew);
            //This sets data-screensize on the body tag AND html tag so it can be referenced by css.
            jQuery('html').attr('data-screensize', screenSizeNew +' '+ sSubBrackets);
            Drupal.settings.screensize.current = screenSizeNew;
            Drupal.settings.screensize.previous = sOldScreenState;
            //Fire any hookJS functions to run just before the screenstate changes and blocks are thrown  - set functions in here hookJS.hook.screenStateChangePre.some_function_key
            hookJS.runHook('screenStateChangePre');
            //track the layout
        	_gaq.push(["_trackEvent", "Site Layout",'responsive-layout-'+screenSizeNew,,, true]);
            //re-run the thrower function calls
            RunThrowCalls();
            //Fire any hookJS functions just after screenstate changes and blocks are thrown  - set functions in here hookJS.hook.screenStateChange.some_function_key
            hookJS.runHook('screenStateChange');

        }
	}

};

;function ThrowBlock (sBlockName, aStates, sTargetID, bDoNow) {
     var sSanitizedBlockName = sBlockName.replace(":","-");
     var sSanitizedBlockName = sSanitizedBlockName.replace(/_/g,"-");
     var bDoNow = bDoNow || false;
     var sTargetID = sTargetID || "target-" + sSanitizedBlockName;
	 var sTargetInnerClass =  'thrown_' + sTargetID;
	 var sUniqueBlockID = sSanitizedBlockName + '-to-' + sTargetID;

    //see if it should throw it now or just store the callback
    if (bDoNow == 'true') {
        //means it should do it now
        //examine the states and see if it should throw the block
        var presence = aStates.indexOf(Drupal.settings.screensize.current);
        if (presence != -1) {
            //the current size is within aStates
            sMessage = 'Throwing ' + sBlockName + ' into ' + sTargetID;
            consoleMe(sMessage);
            jQuery("#" + sTargetID).html('<div class="' + sTargetInnerClass + '">' + Drupal.settings.local_responsive[sBlockName] + '</div>');

        } else {
            //the current state is not in the throw request so eat the block contents
            sMessage = 'Eating ' + sBlockName + ' from ' + sTargetID;
            consoleMe(sMessage);
            jQuery('#' + sTargetID + '  .' + sTargetInnerClass).remove();
        }//end check for presence
    } // end check for bDoNow
    //save a copy of this function call in Drupal.settings.local_responsive.throwCalls
    Drupal.settings.local_responsive.throwCalls[sUniqueBlockID] = 'ThrowBlock (\'' + sBlockName + '\', \'' + aStates + '\', \'' + sTargetID + '\', \'true\');';
};


;function ThrowBlockAjax (sModuleNameAJX, sBlockNameAJX, aStatesAJX, sTargetIDsAJX, bDoNowAJX) {
     var bDoNowAJX = bDoNowAJX || false;
     var sModuleNameAJX = sModuleNameAJX || '';
     var sBlockNameAJX = sBlockNameAJX || '';
     var sTargetIDsAJX = sTargetIDsAJX || '';
     var aStatesAJX = aStatesAJX || '';
     var  sUniqueBlockName = 'AJX-' + sModuleNameAJX + ':' + sBlockNameAJX;
     var sUniqueCallBackName = 'AJX-' + sModuleNameAJX + '-' + sBlockNameAJX + '-to-';
     var  sUniqueCallBackName = sUniqueCallBackName.replace(/_/g,"-")  + sTargetIDsAJX;

    //see if it should throw it now or just store the callback
    if (bDoNowAJX == 'true') {
            //examine the states and see if it should throw the block
            var presence = aStatesAJX.indexOf(Drupal.settings.screensize.current);
            if ((presence != -1) && (!!sModuleNameAJX) && (!!sBlockNameAJX) && (!!sTargetIDsAJX)) {
                //the current size is within aStatesAJX and it contains all the necessary params
               sMessage = 'AJAX Throwing ' + sModuleNameAJX + ':' + sBlockNameAJX+ ' into ' + sTargetIDsAJX;
               consoleMe(sMessage);
                //check to see if we already have the block has already been ajaxed into block data
                if (!!Drupal.settings.local_responsive[sUniqueBlockName]) {
                    //it exists so read throw it from data
                    ThrowBlock(sUniqueBlockName, aStatesAJX, sTargetIDsAJX, 'true');

                } else {
                    //it does not exist so ajax load it and save into data for the next time.
                    //jQuery("#" + sTargetIDsAJX).load('ajax/block/' + sModuleNameAJX + '/' + sBlockNameAJX, function() {
                        jQuery.get('/ajax/block/' + sModuleNameAJX + '/' + sBlockNameAJX, null, function(data,status){
                          if (status == 'success'){
                              //save it to data
                              Drupal.settings.local_responsive[sUniqueBlockName] = data;

                             //TODO it is possible that the carriage returns here and others in Drupal.settings.local_responsive may create some issues in IE
                              sMessage = 'AJAX Load Success: Data now in Drupal.settings.local_responsive.' + sUniqueBlockName;
                              consoleMe(sMessage);
                              //throw it
                              ThrowBlock(sUniqueBlockName, aStatesAJX, sTargetIDsAJX, 'true');
                          } else {
                              sMessage = 'AJAX Load FAILED: ' + sUniqueBlockName;
                              consoleMe(sMessage);
                              //save a copy of this function call in Drupal.settings.local_responsive.throwCalls  for retry next resize
                              Drupal.settings.local_responsive.throwCalls[sUniqueCallBackName] = 'ThrowBlockAjax (\'' + sModuleNameAJX + '\', \'' + sBlockNameAJX + '\', \'' + aStatesAJX + '\', \'' + sTargetIDsAJX + '\', \'true\');';

                          }
                        });
                }
            } else {
                //This is not the right state so just make sure this function call gets saved for resize by
                //putting this function call in Drupal.settings.local_responsive.throwCalls

                Drupal.settings.local_responsive.throwCalls[sUniqueCallBackName] = 'ThrowBlockAjax (\'' + sModuleNameAJX + '\', \'' + sBlockNameAJX + '\', \'' + aStatesAJX + '\', \'' + sTargetIDsAJX + '\', \'true\');';

            } //end check for data exists
    } else {
        //This is not bDoNowAJX so just save it into Drupal.settings.local_responsive.throwCalls
        Drupal.settings.local_responsive.throwCalls[sUniqueCallBackName] = 'ThrowBlockAjax (\'' + sModuleNameAJX + '\', \'' + sBlockNameAJX + '\', \'' + aStatesAJX + '\', \'' + sTargetIDsAJX + '\', \'true\');';
    }// end check for bDoNowAJX
};